
## Description
This one-time project was created to **gather sample data from different movie databases**.

The tool filters required months and years on CSFD, and analyzes individual movies.
When a movie on CSFD is from USA, save the details, and find the movie on filmratings.com for comparison.


The original task was to find whether **age restrictions for a movie are the same in USA and CR**.
That means, try to find movies from defined years, which got different age rating in USA and CR.

sources:

CR - csfd.cz

USA - filmratings.com

## Usage

 - For a proper run, phantomJS with the downloadPage.js script is needed.
 - Change the pathToPhantomJS property in Ratings.java to lead to your phantomJS folder.
 - run the Ratings.java (or change the time span you want to analyze)


## Challenges
 - Despite the fact that the web pages are generated, there are some differences in the structure from time to time which can break the automation
 - Filmratings generates some details in javascript, so it needs to be handled properly (here rendered by PhanomJS)
 - Age span for restrictions differs for each country, so some groups mapping US <-> CR had to be done
 - Movie names with special characters are harder to match generally, some movies are distributed under different names. -> only some minor optimization performed

## Improvements
What could be done better if aimed for production instead of local one-time usage

 - use proper xml parser instead of plaintext parsing
 - use properties file instead of hardcoded string/constants/urls/paths
 - make some performance optimization/threads
 - Split the classes, do them more generic when other countries ratings would be involved
 - Use proper loggers for different messages instead of simple printing
