package movieRatings;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Csfd {

    public static String parseOriginState(BufferedReader br) throws IOException {
        br.reset();
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("class=\"origin\"")) {
                int start = line.indexOf(">");
                String res = line.substring(start + 1);
                int end = res.indexOf("<");
                res = res.substring(0, end - 2);
                return res;
            }
        }
        return "";
    }

    private static ArrayList<String> getMoviesFromOneMonth(int year, int month) {
        String movieIdentifier = "class=\"film c";
        String url = Ratings.monthFilterTemplate.replace("YYYY", String.valueOf(year));
        url = url.replace("MMMM", String.valueOf(month));
        ArrayList<String> results = new ArrayList<>();
        try (final BufferedReader brMonth = Utilities.getBufferFromUrl(url)) {
            String line = "";
            while ((line = brMonth.readLine()) != null) {
                if (line.contains(movieIdentifier)) {
                    String movie = line.split("href=")[1];
                    movie = movie.split("film/")[1];
                    movie = movie.split("/")[0];
                    results.add("https://www.csfd.cz/film/" + movie + "/prehled/");
                }
            }
        } catch (IOException e) {
        }
        return results;
    }

    public static String parseTitle(BufferedReader br) throws IOException {
        br.reset();
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("itemprop=\"name\"")) {
                String res = br.readLine().trim();
                return res;
            }
        }
        return "";
    }

    public static String parseVKinech(BufferedReader br) throws IOException {
        br.reset();
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("title=\"\u010cesko\"") && line.contains("images/flags")) {
                while ((line = br.readLine()) != null) {
                    if (line.contains("class=\"date\"")) {
                        line = br.readLine();
                        int space = line.indexOf(" ");
                        String res = line.substring(0, space);
                        return res;
                    }
                }
                int firstBrace = line.indexOf(">");
                String res = line.substring(firstBrace + 1);
                int end = res.indexOf("<");
                res = res.substring(0, end);
                return res;
            }
        }
        return "";
    }

    //get movies through all defined years and months
    public static void getAllMoviesFromCinemaList() {
        for (int y = Ratings.fromYear; y <= Ratings.toYear; y++) {
            for (int m = Ratings.fromMonth; m <= Ratings.toMonth; m++) {
                ArrayList<String> movieLinks = getMoviesFromOneMonth(y, m);
                for (String mov : movieLinks) {
                    getMovieFromLink(mov);
                }
            }
            Ratings.fromMonth = 1;
        }
    }

    public static String parseGenre(BufferedReader br) throws IOException {
        br.reset();
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("class=\"genre\"")) {
                int firstBrace = line.indexOf(">");
                String res = line.substring(firstBrace + 1);
                int end = res.indexOf("<");
                res = res.substring(0, end);
                return res;
            }
        }
        return "";
    }

    public static String parseCZName(BufferedReader br) throws IOException {
        br.reset();
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("title=\"\u010cesko\"")) {
                line = br.readLine();
                if (line.contains("<h3>")) {
                    String res = line.replace("<h3>", "");
                    res = res.replace("</h3>", "");
                    return res;
                }
            }
        }
        return "";
    }

    public static String parseUSName(BufferedReader br) throws IOException {
        br.reset();
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("title=\"USA\"") || line.contains("Velk\u00e1 Brit\u00e1nie") || line.contains("anglick\u00fd")) {
                line = br.readLine();
                if (line.contains("<h3>")) {
                    String res = line.replace("<h3>", "");
                    res = res.replace("</h3>", "");
                    res = res.replace("&amp;", "&");
                    return res;
                }
            }
        }
        return "";
    }

    public static String parseCzRating(BufferedReader br) throws IOException {
        br.reset();
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("class=\"classification\"")) {
                line = br.readLine();
                String res = line.trim();
                return res;
            }
        }
        return "";
    }

    public static String parseOriginYear(BufferedReader br) throws IOException {
        br.reset();
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("itemprop=\"dateCreated\">")) {
                int firstBrace = line.indexOf(">");
                String res = line.substring(firstBrace + 1);
                int secBrace = res.indexOf(">");
                res = res.substring(secBrace + 1);
                int end = res.indexOf("<");
                res = res.substring(0, end);
                return res;
            }
        }
        return "";
    }

    public static void getMovieFromLink(String urlStr) {
        try {
            BufferedReader brCz = Utilities.getBufferFromUrl(urlStr);
            String line;
            while ((line = brCz.readLine()) != null) {
            }
            String cZName = Csfd.parseCZName(brCz).trim();
            if (cZName == null || cZName.equals("")) {
                cZName = Csfd.parseTitle(brCz).trim();
            }
            String usName = Csfd.parseUSName(brCz).trim();
            if (usName == null || usName.isEmpty()) {
                Utilities.log(cZName + "\t N/Y - 1");
                return;
            }
            String state = Csfd.parseOriginState(brCz).trim();
            if (!state.contains("USA") && state != null) {
                Utilities.log("N/Y - 2");
                return;
            }
            //Get all information from Czech database
            String year = Csfd.parseOriginYear(brCz).trim();
            String genre = Csfd.parseGenre(brCz).trim();
            String vKinech = Csfd.parseVKinech(brCz).trim();
            String cZrating = Csfd.parseCzRating(brCz).trim();
            
            //Get US name of the movie, and find the US rating for it
            String standardizedName = getRatingUrl(usName);
            usRating usR = downloadAndParseUsRating(standardizedName, usName, year);
            String usRating = usR != null ? usR.getRating() : "";
            String usReason = usR != null ? usR.getReason() : "";
            System.out.println(cZName + "\t" + usName + "\t" + state + "\t" + year + "\t" + genre + "\t" + vKinech + "\t" + cZrating + "\t" + usRating + "\t" + usReason);
        } catch (IOException ex) {
            //just skip the movie if there is any problem
        }
    }

    public static String getRatingUrl(String movieName) {
        String movieAsUrl = movieName.trim().replace(" ", "+");
        return movieAsUrl;
    }

    public static usRating downloadAndParseUsRating(String usStdName, String usName, String year) throws IOException {
        if (usStdName.contains("#") || usStdName.contains("?")) {
            return null;
        }
        usRating rating = new usRating();
        phantomRunner runner = new phantomRunner(Ratings.pathToPhantomJS);
        runner.execute(usStdName);
        File htmlFile = new File("download\\" + usStdName + ".html");
        if (htmlFile != null) {
            UsRatingsPage page = new UsRatingsPage(htmlFile);
            rating = page.getMovieRating(usName, year);
            return rating;
        } else {
            return null;
        }
    }
    
}
