package movieRatings;

public class Ratings {
    
    //CONFIGURATION PART
    //--------------------------------------------------------------------------
    public static final String pathToPhantomJS = "D:\\install\\phantomjs\\bin\\";
    //private static final String ratingsUrl = "https://www.filmratings.com/Search?filmTitle=";
    //https://www.csfd.cz/kino/premiery/?country=1&year=2007&month=1&_form_=cinema
    public static final String monthFilterTemplate = "https://www.csfd.cz/kino/premiery/?country=1&year=YYYY&month=MMMM&_form_=cinema";

    public static final int fromYear = 2015; //2007
    public static final int toYear = 2017;    //2017

    public static int fromMonth = 2;    //1
    public static final int toMonth = 12;     //12
    //--------------------------------------------------------------------------
    //CONFIGURATION PART
    
    
    public static void main(String[] args) {
        Csfd.getAllMoviesFromCinemaList();
    }

}
