package movieRatings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class UsRatingsPage {

    String[] allRatings = null;

    public UsRatingsPage(File htmlFile) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(htmlFile));
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("search_result")) {
                allRatings = splitRatingsPerMovie(line);
                break;
            }
        }
        
        
        
        
    }
    public usRating getMovieRating(String name, String year){
    usRating emptyRating = new usRating();
    if(allRatings == null) return emptyRating; //nenasel jsem na strance info o ratingu nebo sem neco spatne nacetl
     
    usRating rating = getCorrectMovieFromListOfRatings(allRatings, name, year);
    if(rating == null) return emptyRating;
    return rating;
    }
        
    public static String[] splitRatingsPerMovie(String results){
        return results.split("<div class=\"resultRow\">");
    }
    
    public static usRating getCorrectMovieFromListOfRatings(String[] movies, String name, String year){
        for(String movie: movies){
            String lower = movie.toLowerCase();
            if(!lower.contains(year)) continue;
            
            String tmp[] = movie.split("class=\"resultData _filmTitle topRow\"");
            
            //get name with date
            String parsedName = tmp[1].split(">")[1];
            parsedName = parsedName.split("<")[0];
 //           System.out.println(parsedName);
            
            //split date
            String parsedYear = parsedName.split("\\(")[1];
            parsedYear = parsedYear.split("\\)")[0];
 //           System.out.println(parsedYear);
            
            parsedName = parsedName.split("\\(")[0].trim();
 //           System.out.println("s");
            
            //rating nazev obsahuje vsechny slova US nazvu z csfd
            String[] parts = name.toLowerCase().replaceAll("[0-9]", "").replace("the ", "").replace("a ", "").replace(".", " ").replace("and ", " ").replace("& ", " ").replace("  ", " ").split(" ");
            
            boolean match = true;
            for(String part:parts){
                if(!parsedName.toLowerCase().contains(part.trim()))
                    match = false;
            }
          if(match && parsedYear.trim().contains(year)){
          //  if(name.toLowerCase().trim().equals(parsedName.toLowerCase().trim()) && year.trim().equals(parsedYear.trim()))
                return getRatingFromMovie(movie);
          
          }
                
        }
        
    return null;
    }
    
    public static usRating getRatingFromMovie(String movie){
        String rating = movie.split("class=\"resultData _filmRating\"")[1];
        rating = rating.split(">")[1];
        rating = rating.split("<")[0];
        
        String reason = movie.split("REASON:")[1];
        reason = reason.split("class=\"resultData\"")[1];
        reason = reason.split(">")[1];
        reason = reason.split("<")[0];
        reason = reason.replace("&nbsp;", "");
        
        return new usRating(rating, reason);
    }

}
