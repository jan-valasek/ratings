package movieRatings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class Utilities {

    public static void log(String msg) {
        //   System.out.println(msg);
    }

    public static BufferedReader getBufferFromUrl(String urlStr) {
        BufferedReader br = null;
        try {
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            HttpURLConnection.setFollowRedirects(true);
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            String encoding = conn.getContentEncoding();
            InputStream inStr;
            if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
                inStr = new GZIPInputStream(conn.getInputStream());
            } else if (encoding != null && encoding.equalsIgnoreCase("deflate")) {
                inStr = new InflaterInputStream(conn.getInputStream(), new Inflater(true));
            } else {
                inStr = conn.getInputStream();
            }
            br = new BufferedReader(new InputStreamReader(inStr, StandardCharsets.UTF_8));
            br.mark(99999999);
        } catch (IOException e) {
        }
        return br;
    }
    
}
