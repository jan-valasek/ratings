package movieRatings;

public class usRating {
    private String rating = "";
    private String reason = "";
    
    public usRating(){};
    public usRating(String rating, String reason) {
        this.rating = rating;
        this.reason = reason;
    }
    
    public String getRating(){
    return this.rating.trim();
    }
    
    public String getReason(){
    return this.reason.trim();
    }
    
}
